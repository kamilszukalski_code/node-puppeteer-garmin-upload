import path from 'path';

export const UPLOAD_PATH = path.join('upload');
export const GENERATED_PATH = 'generated';
export const SCREENSHOTS_PATH = path.join(GENERATED_PATH, 'screenshots');
export const COOKIES_PATH = path.join(GENERATED_PATH, 'cookies');
export const COOKIES_FILE_PATH = path.join(COOKIES_PATH, 'cookies.json');

export const SINGLE_FIT_FILE_LOADING_BINARY_DATA_TIME = 3000;
export const SINGLE_FIT_FILE_PROCESSING_TIME = 4000;

export const GARMIN_URL_UPLOAD = 'https://connect.garmin.com/modern/import-data';
export const GARMIN_URL_SIGNIN = 'https://connect.garmin.com/signin/';

export const loginGarmin = process.env.GARMIN_CONNECT_LOGIN || '';
export const passwordGarmin = process.env.GARMIN_CONNECT_PASSWORD || '';
