import path from 'path';
import {createLogger, format, transports} from 'winston';

const LOGS_PATH = 'logs';
const LOGS_REJECTION_FILE = path.join(LOGS_PATH, 'rejections.log');
const LOGS_EXCEPTIONS_FILE = path.join(LOGS_PATH, 'exceptions.log');
const LOGS_FILE = path.join(LOGS_PATH, 'logs.log');

const logLevels = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
  trace: 5,
};

const fileFormat = format.combine(format.timestamp(), format.json());
const consoleFormat = format.combine(
  format.colorize(),
  format.timestamp(),
  format.splat(),
  format.printf(({level, message, timestamp}) => {
    return `${timestamp} ${level}: ${JSON.stringify(message)} `;
  })
);

export const logger = createLogger({
  level: 'debug',
  levels: logLevels,
  transports: [
    new transports.Console({format: consoleFormat}),
    new transports.File({filename: LOGS_FILE, format: fileFormat}),
  ],
  exceptionHandlers: [
    new transports.Console({format: consoleFormat}),
    new transports.File({filename: LOGS_EXCEPTIONS_FILE, format: fileFormat}),
  ],
  rejectionHandlers: [
    new transports.Console({format: consoleFormat}),
    new transports.File({filename: LOGS_REJECTION_FILE, format: fileFormat}),
  ],
});
