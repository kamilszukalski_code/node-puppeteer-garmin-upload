import {assign, createMachine} from 'xstate';
import initBrowserSrc, {InitBrowserSrcPromiseData} from './states/initBrowser';
import loginPageRoutineSrc, {LoginPageRoutineSrcPromiseData} from './states/loginPageRoutine';
import attachCookieSrc, {AttachCookieSrcPromiseData} from './states/uploadPageVisit/states/attachCookie';
import {initialContext, MachineContext} from './types';
import {actions} from 'xstate';
import {COOKIES_FILE_PATH} from '../config';
import fs from 'fs-extra';
import navigateToUploadPageSrc, {
  NavigateToUploadPageSrcPromiseData,
} from './states/uploadPageVisit/states/navigateToUploadPage';
import clearCookiesSrc, {ClearCookiesSrcPromiseData} from './states/clearCookies';
import {isCookieAttached, isCookiePersisted, isOverMaxRetries} from './gurads';
import uploadPageRoutineSrc, {UploadPageRoutineSrcPromiseData} from './states/uploadPageRoutine';

export const garminMachine =
  /** @xstate-layout N4IgpgJg5mDOIC5QBc7ILIEMDGALAlgHZgB0AkgHJkAqA+gEIBKA8gOoDKAoowMQQD2xEkQBu-ANalUsDDgJDKNBiw7cEo-tkzJ8ggNoAGALqJQAB36x8OwaZAAPRAFoAzAEYATG5IA2AOwArD5u-n4+7j4ANCAAnogBASQAnOEAHAAsqQZu6S5+qUmpAL5F0dKyeESkADLMAOKUtAAKAIJ1nLQsAKrUlJx8gqQakiTlWJVCtQ0UzW0d3b0UnOqEYlo2hIYmSCAWVht2jghOHh6p3um5BT7pPgYGSW7RcQhuhclpIQF+HgYe3y4SmU0ON5DV6o1Wu1Oswen0eGAAE6I-iIkhmAA22gAZqiALajEFyKokKaQuYwuFLFZrbS6TbGOx7az0w7ONznRIBdwcgJZFw+VIeFzPdlnEipFwuYWeU4ZLyA0ogMbEoRdJq1FoAEVm0IAamR2DQSABhAASnBNAGlaC1GB0WtRqC1zZwdSbmMwrWROOweEzLCzbDsjid0j9krk-gKkkk-FK-KKEIKDCRfukku4-DkssKgcqiRNSOrNTqoR0DUbqCRHc7zbQPV6fX6BEJhlJC2CSCXmNrdRXDcbay6zQ3Pd7fTTNHT9IydsyDiH2fGPCRPEkPD5TukAm5Aqkk-l0mmDBn3ALMm4cvmVUXuxre2WKZWh06R2Om76EcjUeischcURAlby7Hs+3LWgX2rYd60bCd2CndZ6S2AN9lZJdjl+FxUz8Aw7nSNwEjeFxMyTHJvBSFxUi3PDJT5Fx0hvTsSTAp99UHasKBaA06kdDpqGYWhWP7AY21WCQOxkUEWIfcDnw4kguJ4vjaAEoTZLY5YNCQ2dtnMQNF1AUMMj8XwNy3blM2CQIk0lbw-FuYJsj3EIDD8JipNVYsNP7SCFKUsheOofjBOE8tvxRNFMRxfFCU8u8wvkqtFO4wKVLUxL2kQmcGT03YDPQoznAyHw13MvlPFuIIk3DY83AYrcfHMjkHkYpUQJJE1qk4O0P3g0ShnEkYOqELqesYPrm2yjYUPnArgyK45zhcNccm5AwYwKPkkyccJVxSO4IhcBJMg8DyKi7Mberg5sIt-aKANikbSCuiabsnbSctm-S0IWhxnEuVISGzVIslyNw8IzdIk0zEgqICKyPDPAUt3O6S1R8iCFnhVtBrEYbmIx0tfOx6lPpmucfqDQg2WOcMVp3dwknSDxmdSQJodiRAIYSYGGN3BUAgeB40a8+9iax2FFn6JFIr-GKgLii6ZIlilSa08SdNy1Dqdppxwwo7l6tlYVLgCJMGKBuN8PSU8HOwtqlUIfgIDgOxnvIKg6CYNguEYHXDP+453F+NNUiF6VHJZnbTgZ0Gch5fJTylUW7zJGZJapTgA8KoOTlwxJsx8AJL2Fflza544fBuZJmfq7MzgSY7U9AzGkuNV1rVte1bTfV13XHZsc7+0NG5WwUsmo7lwySCuXhLiit1tuMOVyaUW5Vx9fKgms+9Hd72GHmmMJOAxd2BsILwSM+mpFSv5RINzbg3erLjORVgXi1vVfY5KAqCkK6kf7Zzmr9Y+i0nB7nDo-Ne+RcIszgUmPkqYzjxgRhmLwGY3AbyJlvCCUEj561QaZCeBgp4MT8LPGqgoPjCian8SUZwUg4Jet1a6g9fSEJPuHfabxY4bSFtmJ4ldCIrXuAUM+wpsKz1OCw8WeC1ZSz6FwiBZ5H6mwyARJGmQoiV0oUkYGjk9y7m5MENqn9lZCE4BQHU7AugmhNL6Q+oDdYYVOKucMPx+bW3yHPbmp4DGmwzI8UGwpmZyOsTqAAYi0Mg1Quj2hUUHU4plLh3F3ByHMAR-g7UlHDKUpxMwrkeMw9qhMwBJNDO4HcvhAjBFCOEEIO14ymVjARdw3xYwMJKCUIAA */
  createMachine(
    {
      context: initialContext,
      tsTypes: {} as import('./garminMachine.typegen').Typegen0,
      schema: {
        context: {} as MachineContext,
        services: {} as {
          initBrowserSrc: {data: InitBrowserSrcPromiseData};
          loginPageRoutineSrc: {data: LoginPageRoutineSrcPromiseData};
          attachCookieSrc: {data: AttachCookieSrcPromiseData};
          clearCookiesSrc: {data: ClearCookiesSrcPromiseData};
          navigateToUploadPageSrc: {data: NavigateToUploadPageSrcPromiseData};
          uploadPageRoutineSrc: {data: UploadPageRoutineSrcPromiseData};
        },
      },
      preserveActionOrder: true,
      initial: 'INIT_BROWSER',
      id: 'testMachine',
      states: {
        INIT_BROWSER: {
          invoke: {
            src: 'initBrowserSrc',
            onDone: [
              {
                actions: ['assignInitData', 'assignCookieFromFile'],
                cond: 'isCookiePersisted',
                target: 'UPLOAD_PAGE_VISIT',
              },
              {
                actions: 'assignInitData',
                target: 'LOGIN_PAGE_ROUTINE',
              },
            ],
          },
        },
        LOGIN_PAGE_ROUTINE: {
          invoke: {
            src: 'loginPageRoutineSrc',
            onDone: [
              {
                actions: ['pureStoreCookieInFile', 'assignCookieFromFile', 'assignIsCookieAttached'],
                target: 'UPLOAD_PAGE_VISIT',
              },
            ],
            onError: [
              {
                cond: 'isOverMaxRetries',
                target: 'END_FAILURE',
              },
              {
                actions: 'assignIncreaseCurrentRetryAction',
                target: 'CLEAR_COOKIES',
              },
            ],
          },
        },
        UPLOAD_PAGE_VISIT: {
          initial: 'CHECK_ARE_ATTACHED_COOKIES',
          states: {
            CHECK_ARE_ATTACHED_COOKIES: {
              always: [
                {
                  cond: 'isCookieAttached',
                  target: 'NAVIGATE_TO_UPLOAD_PAGE',
                },
                {
                  target: 'ATTACH_COOKIES',
                },
              ],
            },
            ATTACH_COOKIES: {
              invoke: {
                src: 'attachCookieSrc',
                onDone: [
                  {
                    target: 'NAVIGATE_TO_UPLOAD_PAGE',
                  },
                ],
                onError: [
                  {
                    target: '#testMachine.LOGIN_PAGE_ROUTINE',
                  },
                ],
              },
            },
            NAVIGATE_TO_UPLOAD_PAGE: {
              invoke: {
                src: 'navigateToUploadPageSrc',
                onDone: [
                  {
                    target: '#testMachine.UPLOAD_PAGE_ROUTINE',
                  },
                ],
                onError: [
                  {
                    target: '#testMachine.CLEAR_COOKIES',
                  },
                ],
              },
            },
          },
        },
        CLEAR_COOKIES: {
          exit: ['assignClearCookieAction', 'assignIsCookieAttached'],
          invoke: {
            src: 'clearCookiesSrc',
            onDone: [
              {
                target: 'LOGIN_PAGE_ROUTINE',
              },
            ],
            onError: [
              {
                target: 'END_FAILURE',
              },
            ],
          },
        },
        UPLOAD_PAGE_ROUTINE: {
          invoke: {
            src: 'uploadPageRoutineSrc',
            onDone: [
              {
                target: 'END_SUCCESS',
              },
            ],
            onError: [
              {
                target: 'CLEAR_COOKIES',
              },
            ],
          },
        },
        END_SUCCESS: {
          type: 'final',
        },
        END_FAILURE: {
          type: 'final',
        },
      },
    },
    {
      actions: {
        assignInitData: assign((c, e) => {
          return {
            browser: e.data.browser,
            page: e.data.page,
            filesToUpload: e.data.filesToUpload,
          };
        }),

        assignCookieFromFile: assign(c => {
          try {
            console.log('[Action][Assign]: assignCookieFromFile');
            const cookiesFromDisk = fs.readJSONSync(COOKIES_FILE_PATH);
            return {cookies: cookiesFromDisk};
          } catch (e) {
            return {cookies: undefined};
          }
        }),

        assignClearCookieAction: assign((c: MachineContext) => {
          return {cookies: undefined};
        }),

        assignIncreaseCurrentRetryAction: assign((c: MachineContext) => {
          return {currentRetry: c.currentRetry + 1};
        }),

        assignIsCookieAttached: assign({
          isCookieAttached: context => {
            console.log('[Action][Assign]: assignCookieAttachement');
            return context.cookies ? true : false;
          },
        }),

        pureStoreCookieInFile: actions.pure((c, e) => {
          if (c.page) {
            console.log('[Action][Pure]: pureStoreCookieInFile');
            fs.outputFileSync(COOKIES_FILE_PATH, JSON.stringify(e.data.cookies, null, 2));
            return undefined;
          }
        }),
      },
      services: {
        initBrowserSrc,
        loginPageRoutineSrc,
        attachCookieSrc,
        navigateToUploadPageSrc,
        clearCookiesSrc,
        uploadPageRoutineSrc,
      },
      guards: {
        isCookiePersisted,
        isCookieAttached,
        isOverMaxRetries,
      },
    }
  );
