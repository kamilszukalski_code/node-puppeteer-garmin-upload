import {Browser, Page} from 'puppeteer';
import {assign, Assigner, PropertyAssigner, StateNode} from 'xstate';

export interface MachineContext {
  cookies?: any;
  isCookieAttached: boolean;
  successElements: Set<string>;
  errorElements: Set<string>;
  filesToUpload: string[];
  maxRetries: number;
  currentRetry: number;
  browser?: Browser;
  page?: Page;
}

export const initialContext: MachineContext = {
  errorElements: new Set<string>(),
  isCookieAttached: false,
  filesToUpload: [],
  successElements: new Set<string>(),
  maxRetries: 3,
  currentRetry: 0,
};

export enum MachineStates {
  'INIT_BROWSER' = 'INIT_BROWSER',
  'LOGIN_PAGE_ROUTINE' = 'LOGIN_PAGE_ROUTINE',
  'UPLOAD_PAGE_VISIT' = 'UPLOAD_PAGE_VISIT',
  'UPLOAD_PAGE_ROUTINE' = 'UPLOAD_PAGE_ROUTINE',
  'END_SUCCESS' = 'END_SUCCESS',
  'END_FAILURE' = 'END_FAILURE',
}

export enum MachineActions {
  // 'LOAD_COOKIES' = 'LOAD_COOKIES',
  // 'SAVE_COOKIES' = 'SAVE_COOKIES',
  // 'LOGIN_ROUTINE' = 'LOGIN_ROUTINE',
  // 'UPLOAD_ROUTINE' = 'UPLOAD_ROUTINE',
  // 'VISIT_LOGIN_PAGE' = 'VISIT_LOGIN_PAGE',
  // 'VISIT_UPLOAD_PAGE' = 'VISIT_UPLOAD_PAGE',
  'COMPLETE_ROUTINE' = 'COMPLETE_ROUTINE',
}

export type MachineActionsType =
  // | {type: MachineActions.LOAD_COOKIES}
  // | {type: MachineActions.SAVE_COOKIES}
  // | {type: MachineActions.LOGIN_ROUTINE}
  // | {type: MachineActions.UPLOAD_ROUTINE}
  // | {type: MachineActions.VISIT_LOGIN_PAGE}
  // | {type: MachineActions.VISIT_UPLOAD_PAGE}
  {type: MachineActions.COMPLETE_ROUTINE};

export type MachineStatesType =
  | {value: MachineStates.INIT_BROWSER; context: MachineContext}
  | {value: MachineStates.LOGIN_PAGE_ROUTINE; context: MachineContext}
  | {value: MachineStates.UPLOAD_PAGE_VISIT; context: MachineContext}
  | {value: MachineStates.UPLOAD_PAGE_ROUTINE; context: MachineContext}
  | {value: MachineStates.END_SUCCESS; context: MachineContext}
  | {value: MachineStates.END_FAILURE; context: MachineContext};

export interface MachineStatesSchema {
  states: {
    [MachineStates.INIT_BROWSER]: StateNode;
    [MachineStates.LOGIN_PAGE_ROUTINE]: StateNode;
    [MachineStates.UPLOAD_PAGE_ROUTINE]: StateNode;
    [MachineStates.UPLOAD_PAGE_VISIT]: StateNode;
    [MachineStates.END_FAILURE]: StateNode;
    [MachineStates.END_SUCCESS]: StateNode;
  };
}

export const garminAssign = (assignment: Assigner<MachineContext, any> | PropertyAssigner<MachineContext, any>) =>
  assign<MachineContext, any>(assignment);
