import {existsSync} from 'fs-extra';
import {COOKIES_FILE_PATH} from '../config';
import {MachineContext} from './types';

export const isCookiePersisted = () => {
  console.log('[GUARD]: isCookiePersisted', existsSync(COOKIES_FILE_PATH));
  return existsSync(COOKIES_FILE_PATH);
};

export const isCookieAttached = (context: MachineContext) => {
  console.log('[GUARD]: isCookieAttached', context.isCookieAttached);
  return context.isCookieAttached;
};

export const isOverMaxRetries = (context: MachineContext): boolean => {
  console.log('[GUARD]: isOverMaxRetries', context.isCookieAttached);
  return context.currentRetry >= context.maxRetries;
};
