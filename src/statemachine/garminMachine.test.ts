import {interpret, StateValue} from 'xstate';

import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import {Browser, Page} from 'puppeteer';
import {garminMachine} from './garminMachine';

describe('garminUploadMachine', () => {
  let browser: Browser | undefined;
  let page: Page | undefined;
  let config: any;
  let events: (string | StateValue)[] = [];

  const createBaseConfig = (browser: Browser, page: Page) => ({
    services: {
      initBrowserSrc: () => {
        return Promise.resolve({browser, page, filesToUpload: []});
      },
    },
  });

  const createInterpreterWithEventsLog = (machine: typeof garminMachine) =>
    interpret(machine).onTransition(state => {
      events.push(state.value);
    });

  beforeEach(async () => {
    browser = await puppeteer.use(StealthPlugin()).launch({headless: true});
    page = await browser.newPage();
    await page.setViewport({width: 1920, height: 937, deviceScaleFactor: 1});

    config = createBaseConfig(browser, page);
    events = [];
  });

  afterEach(async () => {
    browser && (await browser.close());
  });

  it(
    'should go to END_FAILURE directly',
    async () => {
      const mockedMachine = garminMachine.withConfig({
        actions: {
          pureStoreCookieInFile: () => {},
          assignCookieFromFile: () => {},
          assignIsCookieAttached: () => {},
        },
        guards: {
          ...config.guards,
          isCookiePersisted: () => false,
          isOverMaxRetries: () => true,
          isCookieAttached: () => false,
        },
        services: {
          ...config.services,
          loginPageRoutineSrc: () => {
            return Promise.reject({cookies: undefined});
          },
          attachCookieSrc: () => Promise.reject(),
          navigateToUploadPageSrc: () => Promise.reject(true),
          clearCookiesSrc: () => Promise.reject(),
          uploadPageRoutineSrc: () => Promise.reject(),
        },
      });

      createInterpreterWithEventsLog(mockedMachine)
        .onDone(async () => {
          expect(events).toEqual(['INIT_BROWSER', 'LOGIN_PAGE_ROUTINE', 'END_FAILURE']);
          return Promise.resolve();
        })
        .start();
    },
    7 * 60 * 1000
  );

  it(
    'should go to Full Route',
    async () => {
      const mockedMachine = garminMachine.withConfig({
        actions: {
          pureStoreCookieInFile: () => {},
          assignCookieFromFile: () => {},
          assignIsCookieAttached: () => {},
        },
        guards: {
          ...config.guards,
          isCookiePersisted: () => false,
          isOverMaxRetries: () => false,
          isCookieAttached: () => true,
        },
        services: {
          ...config.services,
          loginPageRoutineSrc: () => {
            return Promise.resolve({cookies: undefined});
          },
          attachCookieSrc: () => Promise.resolve(),
          navigateToUploadPageSrc: () => Promise.resolve(true),
          clearCookiesSrc: () => Promise.resolve(),
          uploadPageRoutineSrc: () => Promise.resolve(),
        },
      });

      createInterpreterWithEventsLog(mockedMachine)
        .onDone(async () => {
          expect(events).toEqual([
            'INIT_BROWSER',
            'LOGIN_PAGE_ROUTINE',
            {UPLOAD_PAGE_VISIT: 'NAVIGATE_TO_UPLOAD_PAGE'},
            'UPLOAD_PAGE_ROUTINE',
            'END_SUCCESS',
          ]);
          return Promise.resolve();
        })
        .start();
    },
    7 * 60 * 1000
  );

  it(
    'should go to Direct path',
    async () => {
      const mockedMachine = garminMachine.withConfig({
        actions: {
          pureStoreCookieInFile: () => {},
          assignCookieFromFile: () => {},
          assignIsCookieAttached: () => {},
        },
        guards: {
          ...config.guards,
          isCookiePersisted: () => true,
          isOverMaxRetries: () => false,
          isCookieAttached: () => false,
        },
        services: {
          ...config.services,
          loginPageRoutineSrc: () => {
            return Promise.resolve({cookies: {testCokkie: 'adasdsa'}});
          },
          attachCookieSrc: () => Promise.resolve(),
          navigateToUploadPageSrc: () => Promise.resolve(true),
          clearCookiesSrc: () => Promise.resolve(),
          uploadPageRoutineSrc: () => Promise.resolve(),
        },
      });

      createInterpreterWithEventsLog(mockedMachine)
        .onDone(async () => {
          expect(events).toEqual([
            'INIT_BROWSER',
            {UPLOAD_PAGE_VISIT: 'ATTACH_COOKIES'},
            {UPLOAD_PAGE_VISIT: 'NAVIGATE_TO_UPLOAD_PAGE'},
            'UPLOAD_PAGE_ROUTINE',
            'END_SUCCESS',
          ]);
          return Promise.resolve();
        })
        .start();
    },
    7 * 60 * 1000
  );
});
