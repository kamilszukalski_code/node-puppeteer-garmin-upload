import path from 'path';
import fs from 'fs-extra';
import puppeteer from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import {COOKIES_PATH, SCREENSHOTS_PATH, UPLOAD_PATH} from '../../config';
import {Browser, Page} from 'puppeteer';

const initBrowserSrc = async (): Promise<InitBrowserSrcPromiseData> => {
  await fs.remove(SCREENSHOTS_PATH);
  await fs.ensureDir(SCREENSHOTS_PATH);
  await fs.ensureDir(UPLOAD_PATH);
  await fs.ensureDir(COOKIES_PATH);

  const fileNamesToUpload = await fs.readdir(UPLOAD_PATH);
  const filesToUpload = fileNamesToUpload.map(f => path.join(UPLOAD_PATH, f));

  const browser = await puppeteer.use(StealthPlugin()).launch({headless: true});
  const page = await browser.newPage();
  await page.setViewport({width: 1920, height: 937, deviceScaleFactor: 1});

  return Promise.resolve({browser, page, filesToUpload});
};

export interface InitBrowserSrcPromiseData {
  browser: Browser;
  page: Page;
  filesToUpload: string[];
}
export default initBrowserSrc;
