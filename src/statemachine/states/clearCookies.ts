import {logger} from '../../logging/logger';
import {waitForPage} from '../../utils/puppeteer-utils';
import {MachineContext} from '../types';
import fs, {existsSync} from 'fs-extra';
import {COOKIES_FILE_PATH} from '../../config';

const clearCookiesSrc = async (context: MachineContext): Promise<ClearCookiesSrcPromiseData> => {
  const SERVICE_NAME = 'clearCookiesSrc';
  const {page} = context;

  if (!page) {
    logger.error(`[Service: ${SERVICE_NAME}] Could not find page in context`);
    return Promise.reject('Could not find page in context');
  }

  // await page.deleteCookie(); // did not work as expected
  const client = await page.target().createCDPSession();
  await client.send('Network.clearBrowserCookies');
  await waitForPage(page, 300, 700);

  if (existsSync(COOKIES_FILE_PATH)) {
    fs.removeSync(COOKIES_FILE_PATH);
  }

  return Promise.resolve();
};

export type ClearCookiesSrcPromiseData = void;
export default clearCookiesSrc;
