import {GARMIN_URL_SIGNIN, loginGarmin, passwordGarmin, SCREENSHOTS_PATH} from '../../config';
import {MachineContext} from '../types';
import {waitForPage} from '../../utils/puppeteer-utils';
import {logger} from '../../logging/logger';

const loginPageRoutineSrc = async (context: MachineContext): Promise<LoginPageRoutineSrcPromiseData> => {
  const SERVICE_NAME = 'loginPageRoutineSrc';
  const {page, browser} = context;
  if (!page || !browser) {
    logger.error('There is no page or browser');
    return Promise.reject('There is no page in context');
  }

  logger.info(`Visiting page ${GARMIN_URL_SIGNIN}...`);
  await page.goto(GARMIN_URL_SIGNIN, {waitUntil: 'networkidle2'});
  await waitForPage(page, 3000, 5000);

  await page.screenshot({path: `./${SCREENSHOTS_PATH}/00-welcome.png`});
  // const data = await page.content();
  // fs.outputFileSync(`${SCREENSHOTS_PATH}/00-welcome.html`, data);

  const elementHandle = await page.$('#gauth-widget-frame-gauth-widget');
  if (!elementHandle) {
    logger.error(`[Service: ${SERVICE_NAME}] Could not find IFrame`);
    return Promise.reject('Could not find IFrame....');
  }

  const frame = await elementHandle.contentFrame();
  if (!frame) {
    logger.error(`[Service: ${SERVICE_NAME}] Could not load IFrame`);
    return Promise.reject('Could not find IFrame....');
  }

  if (await page.$('#truste-consent-button')) {
    await page.click('#truste-consent-button');
  } else {
    logger.warn(`[Service: ${SERVICE_NAME}] Could not find  Cookies Baner -> probably it was already allowed`);
  }

  logger.info('Typing login...');
  await frame.waitForSelector('#username', {timeout: 3000});
  await frame.type('#username', loginGarmin, {delay: 132});

  logger.info('Typing password...');
  await frame.waitForSelector('#password', {timeout: 3000});
  await frame.type('#password', passwordGarmin, {delay: 325});

  await page.screenshot({path: `./${SCREENSHOTS_PATH}/01-before-login.png`});
  logger.info('Singing in...');
  await frame.click('#login-btn-signin');

  await frame.waitForNavigation();
  await page.screenshot({path: `./${SCREENSHOTS_PATH}/02-after-login.png`});
  await waitForPage(page, 3000, 4000);
  await page.screenshot({path: `./${SCREENSHOTS_PATH}/03-dashboard.png`});

  await waitForPage(page, 3000, 4000);

  const cookies = await page.cookies();

  return Promise.resolve({cookies});
};

export interface LoginPageRoutineSrcPromiseData {
  cookies: any;
}
export default loginPageRoutineSrc;
