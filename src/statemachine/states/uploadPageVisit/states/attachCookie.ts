import {logger} from '../../../../logging/logger';
import {waitForPage} from '../../../../utils/puppeteer-utils';
import {MachineContext} from '../../../types';

const attachCookieSrc = async (context: MachineContext): Promise<AttachCookieSrcPromiseData> => {
  const {page, cookies} = context;

  if (!page) {
    logger.info(`Could not find page in context`);
    return Promise.reject('Could not find page in context');
  }
  if (!cookies) {
    logger.info(`Could not find cookies in context`);
    return Promise.reject('Could not find cookies in context');
  }

  await page.setCookie(...cookies);

  await waitForPage(page, 5000, 6000);

  return Promise.resolve();
};

export type AttachCookieSrcPromiseData = void;
export default attachCookieSrc;
