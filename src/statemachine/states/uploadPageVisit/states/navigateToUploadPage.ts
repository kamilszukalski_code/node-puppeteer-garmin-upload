import {GARMIN_URL_UPLOAD, SCREENSHOTS_PATH} from '../../../../config';
import {logger} from '../../../../logging/logger';
import {waitForPage} from '../../../../utils/puppeteer-utils';
import {MachineContext} from '../../../types';

const navigateToUploadPageSrc = async (context: MachineContext): Promise<NavigateToUploadPageSrcPromiseData> => {
  const {page} = context;
  if (!page) return Promise.reject('Page was not properrly initialized');

  await page.screenshot({path: `./${SCREENSHOTS_PATH}/01.png`});
  logger.info(`Trying to reach ${GARMIN_URL_UPLOAD}...`);
  await page.goto(GARMIN_URL_UPLOAD, {waitUntil: 'networkidle2'});
  await waitForPage(page, 1000, 2000);
  await page.screenshot({path: `./${SCREENSHOTS_PATH}/02.png`});

  if (await page.$('#import-data-start')) {
    await page.screenshot({path: `./${SCREENSHOTS_PATH}/03.png`});
    logger.info(`${GARMIN_URL_UPLOAD} is accessible! `);
    return Promise.resolve(true);
  } else {
    logger.warn(`Could not visit ${GARMIN_URL_UPLOAD} ...`);
    return Promise.reject(false);
  }
};

export type NavigateToUploadPageSrcPromiseData = boolean;
export default navigateToUploadPageSrc;
