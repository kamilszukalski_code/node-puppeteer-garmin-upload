import {
  SCREENSHOTS_PATH,
  SINGLE_FIT_FILE_LOADING_BINARY_DATA_TIME,
  SINGLE_FIT_FILE_PROCESSING_TIME,
} from '../../config';
import {MachineContext, MachineStates} from '../types';
import {isElementVisible, waitForPage} from '../../utils/puppeteer-utils';
import {logger} from '../../logging/logger';

const uploadPageRoutineSrc = async (context: MachineContext): Promise<UploadPageRoutineSrcPromiseData> => {
  const {page, filesToUpload} = context;

  if (!page) return Promise.reject('No PAGE');

  await page.screenshot({path: `./${SCREENSHOTS_PATH}/04-upload-screen.png`});

  const uploadSection = await page.$('input[type=file]');
  if (!uploadSection) {
    await page.screenshot({path: `./${SCREENSHOTS_PATH}/error.png`});
    return Promise.reject(`Could not find input[type=file] in  ${MachineStates.UPLOAD_PAGE_ROUTINE}`);
  }

  await uploadSection.uploadFile(...filesToUpload);

  logger.info('UPDATE_SCREEN => Adding multiple files to upload...');
  await waitForPage(
    page,
    filesToUpload.length * SINGLE_FIT_FILE_LOADING_BINARY_DATA_TIME,
    (filesToUpload.length + 1) * SINGLE_FIT_FILE_LOADING_BINARY_DATA_TIME
  );

  logger.info('UPDATE_SCREEN => Starting process of upload...');
  await page.click('#import-data-start');

  await waitForPage(
    page,
    filesToUpload.length * SINGLE_FIT_FILE_PROCESSING_TIME,
    (filesToUpload.length + 1) * SINGLE_FIT_FILE_PROCESSING_TIME
  );

  logger.info('UPDATE_SCREEN => gathering statistics');
  const rows = await page.$$('tr.dz-row');
  const successElements = new Set<string>();
  const errorElements = new Set<string>();
  for await (const row of rows) {
    const filename = await row.$eval('.dz-filename > span', node => node.textContent);
    const isSuccess = await isElementVisible(await row.$('span.dz-success-mark'), page);

    if (filename && isSuccess) {
      successElements.add(filename);
      continue;
    }

    const isError = await isElementVisible(await row.$('span.dz-error-message'), page);
    if (filename && isError) {
      errorElements.add(filename);
      continue;
    }
  }

  logger.info(`How many success %O`, successElements);
  logger.info(`How many errors %O`, errorElements);
  await page.screenshot({path: `./${SCREENSHOTS_PATH}/05-success.png`});

  return Promise.resolve();
};

export type UploadPageRoutineSrcPromiseData = void;
export default uploadPageRoutineSrc;
