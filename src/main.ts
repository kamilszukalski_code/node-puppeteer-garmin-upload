import {interpret} from 'xstate';
import {garminMachine} from './statemachine/garminMachine';

interpret(garminMachine)
  .onTransition(state => {
    console.log(state.value);
  })
  .start();
