import {ElementHandle, Page} from 'puppeteer';
import {logger} from '../logging/logger';

export const isElementVisible = async (element: ElementHandle | null, page: Page, non0content: boolean = true) => {
  if (!element) return false;

  const isVisibleHandle = await page.evaluateHandle(e => {
    const style = window.getComputedStyle(e);
    return style && style.display !== 'none' && style.visibility !== 'hidden' && style.opacity !== '0';
  }, element);

  var visible = await isVisibleHandle.jsonValue();
  const box = await element.boxModel();

  if (visible && box) {
    if (non0content) {
      return box.width !== 0 && box.height !== 0;
    } else {
      return true;
    }
  }
  return false;
};

export const waitForPage = async (page: Page, min: number, max: number) => {
  const sleepDuration = Math.floor(Math.random() * (max - min) + min);
  logger.debug(`Waiting for ${sleepDuration}`);
  await page.waitForTimeout(sleepDuration);
};
