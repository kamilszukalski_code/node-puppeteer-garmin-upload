#!/bin/sh

npm install typescript dotenv fs-extra ts-node nodemon webpack webpack-cli husky @types/fs-extra @types/app-root-path app-root-path --save-dev


#husky
npm set-script prepare "husky install"
npx husky install
npx husky add .husky/pre-commit "npm-lint"


#scripts
npm set-script compile "webpack"