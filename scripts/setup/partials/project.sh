#!/bin/sh

mkdir src
touch src/main.ts


touch readme.md
touch .env


npm set-script dev "nodemon -r dotenv/config src/main.ts dotenv_config_path=./.env"
npm set-script start "ts-node -r dotenv/config src/main.ts dotenv_config_path=./.env"