#!/bin/sh

NODE_VERSION_MAJOR=16
NODE_VERSION_MINOR=13.2

npm init -y


touch .nvmrc
echo "${NODE_VERSION_MAJOR}.${NODE_VERSION_MINOR}" > .nvmrc
nvm use
npm install @types/node@${NODE_VERSION_MAJOR} @tsconfig/node${NODE_VERSION_MAJOR} --save-dev