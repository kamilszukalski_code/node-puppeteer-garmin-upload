#!/bin/sh

npm i prettier --save-dev
npm set-script prettier "prettier -c ."
npm set-script prettier:fix "prettier -w ."

cat <<EOF > ./.prettierignore
build
coverage
lib
node_modules
build
bundle
docs


generated
inputs
.vscode

# docker files
docker
docker-compose*.yml
*.sh

# env files
.env*
EOF

cat <<EOF > ./.prettierrc.json
{
  "bracketSpacing": false,
  "singleQuote": true,
  "trailingComma": "es5",
  "arrowParens": "avoid",
  "printWidth": 120,
  "endOfLine": "auto"
}
EOF