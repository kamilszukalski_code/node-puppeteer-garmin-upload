#!/bin/sh

mkdir docker
touch ./docker/Dockerfile
touch ./docker/Dockerfile.dockerignore

cat <<EOF > ./docker/Dockerfile.dockerignore
node_modules
dist
build
logs
.husky
docs
output

assets
EOF
