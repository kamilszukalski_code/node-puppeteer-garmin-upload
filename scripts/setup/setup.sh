#!/bin/sh

./scripts/setup/partials/node_nvm.sh
./scripts/setup/partials/docker.sh
./scripts/setup/partials/dependencies.sh
./scripts/setup/partials/dependencies-eslint.sh
./scripts/setup/partials/dependencies-prettier.sh
./scripts/setup/partials/project.sh