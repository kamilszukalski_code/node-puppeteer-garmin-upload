#!/bin/sh

rm -rf .husky
rm -rf docker
rm -rf node_modules
rm -rf src
rm .nvmrc 
rm package*.json

